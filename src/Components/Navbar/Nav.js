import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import grey from '@material-ui/core/colors/grey'
import './Nav.css'

const primary=grey[800]
class Nav extends Component {
     render() {
          return (
               <div style={{backgroundColor:(primary)}} className='nav'>
               <h2 className='nav-band'>BOYCODER.TECH</h2>
                    <nav className="navbar navbar-expand-sm bg-light navbar-light">
                         <ul className="navbar-nav">
                              <li className="nav-item active"><Link to='/'>
                              <a className="nav-link" href="#"> Home</a></Link>
                              </li>
                              <li className="nav-item"><Link to='/about'>
                              <a className="nav-link" href="#">About</a></Link>
                              </li>
                              <li className="nav-item"> <Link to='/project'>
                              <a className="nav-link" href="#"> Project</a></Link>
                              </li>
                         </ul>
               </nav>

               </div>
          )
     }
}

export default Nav
