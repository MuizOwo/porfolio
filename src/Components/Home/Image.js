import React from 'react'
import IMG from '../../assets/coding.png'

function Image() {
     return (
          <div>
               <img src={IMG} height='100%' width='30%' alt='code' />
          </div>
     )
}

export default Image