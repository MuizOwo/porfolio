import React, { Component } from 'react'
import './Home.css'
import Imag from './Imag'
import Image from './Image'
import ContactInfo from './Contact'


export class Home extends Component {
     render() {
          return (
               <div className='wrap'>
               <div className='container'>


                         <div className='image'>
                              <Image  />
                         </div>
                         
                    <div className='heading'>
                         <h1>Hello. I'm BoyCoder!</h1>
                         <h2>I'am front end developer currently studying in Malaysia</h2>
                         <h2>I'm always open to discuss new opportunities, tech trends and meet new people. Don't hesitate on reaching out!</h2>
                         <p>Interested in working together </p>
                         <a className="link" href='https://www.instagram.com/boycoder.tech/' target='blank'> Reach me out </a>
                    </div>

                    <div className='imag'>
                         <div className='img-1'>
                              <Imag />
                         </div>
                    </div>

                    <div className='contact'>
                         <ContactInfo />
                    </div>

               </div>
               <div className='footer'>
                         <div className='font'>
                              <a href='https://www.instagram.com/boycoder.tech/'  target='-blank'><i className="fab fa-instagram fa-2x"></i></a>
                              <a href='https://twitter.com/BoycoderT' target='-blank' ><i class="fab fa-twitter fa-2x"></i></a>
                         </div>
                         <p>Created by BoyCoder.Tech, 2020 </p>
                    </div>
               </div>
          )
     }
}

export default Home
