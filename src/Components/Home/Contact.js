import React, { Component } from 'react'
import grey from '@material-ui/core/colors/grey'
import './Contact.css'

const theme = grey[50]

export class ContactInfo extends Component {
     render() {
          return (
               <div className='c'>
               <div className="card" style={{backgroundColor:(theme)}}>
                    <div className="card-header">
                         Contact Information
                    </div>
                    <ul className="list-group list-group-flush">
                    <li className="list-group-item"> <i className="fas fa-phone-alt"></i>018-386-7619</li>
                    <li className="list-group-item"><i class="fab fa-whatsapp"></i>018-386-6719</li>
                    <li className="list-group-item"><i class="fas fa-envelope"></i>muizowo@gmail.com</li>
                    </ul>
                    </div>
                    </div>
          )
     }
}

export default ContactInfo
