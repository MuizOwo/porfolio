import React, { Component } from 'react'
import './About.css'
import grey from '@material-ui/core/colors/grey'


const primary = grey[50];

export class About extends Component {
     render() {
          return (
               <div className='body'>
               <div className='container '>

                         <div  className='text'>
                              <div className='text-wrap'>
                                   <h2>Hello,</h2>
                                   <p>I am BoyCoder.Tech, web developer from Kuala Lumpur, Malaysia. I have rich experience in web site design and building and customization, also I am good at React.js</p>
                                   <p>My creativity, passion for problem-solving and strong attention to detail would prove valuable on any team of individuals. Striving to work on teams to not only build meaningful products but also learn from those around me.</p>
                              </div>
                         </div>

                         <div className='skills'>
                              <h3>Skills that I know !</h3>
                              <div className='s'>
                                   <div className='s1'>
                                        <p>HTML, CSS, Bootstrap,  Materia UI</p>
                                        <div className="progress">
                                        <div className="progress-bar progress-bar-striped bg-danger" role="progressbar" 
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">90%</div>
                                        </div>
                                   </div>
                                   <div className='s2'>
                                        <p>Reactjs, React Native</p>
                                        <div className="progress">
                                        <div className="progress-bar progress-bar-striped bg-danger" role="progressbar" 
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">60%</div>
                                        </div>
                                   </div>
                                   <div className='s3'>
                                        <p>Git</p>
                                        <div className="progress">
                                        <div className="progress-bar progress-bar-striped bg-danger" role="progressbar"
                                        aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">80%</div>
                                        </div>
                                   </div>
                              </div>
                         </div>

                         <div className='spotify'>
                              <h4>I love listen beautiful songs while coding here is my Spotify Playlist</h4>
                              <iframe
                                   title='spotify'
                                   src="https://open.spotify.com/embed/playlist/77FZohoA4UFeDWPhBbc8Dh" width="300" height="480" frameborder="0" allowtransparency="true" allow="encrypted-media">
                              </iframe>
                         </div>
                    </div>
                    </div>
          )
     }
}

export default About
