import React, { Component } from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Nav from './Components/Navbar/Nav'
import Home from './Components/Home/Home'
import About from './Components/About/About'

class App extends Component {
    render(){
      return (
        <Router>
          <div>
              <Nav />
                <Switch>
                <Route exact path='/' component={Home} />
                  <Route path='/about' component={About} />
                </Switch>
          </div>
        </Router>
      )
  }
}

export default App;
